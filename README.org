* Oglo Arch Repository
OgloTheNerd's Arch Linux repository.

** How To Use
1. Add this to `/etc/pacman.conf`:

#+begin_src
[oglo-arch-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/Oglo12/$repo/-/raw/main/$arch
#+end_src

2. Run this command:
#+begin_src bash
sudo pacman -Syy
#+end_src
